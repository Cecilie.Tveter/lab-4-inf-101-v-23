package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {
    IColorGrid colorGrid;
    private static final double OUTERMARGIN = 30;
    private static final Color marginColor = Color.LIGHT_GRAY;
    private static final double margin = 30;
    private static final double x = margin;
    private static final double y = margin;
    private static final Color FRAMECOLOR = Color.darkGray;


    public GridView () {
        this.setPreferredSize(new Dimension(400, 300));
        this.colorGrid = new ColorGrid(3, 4);

    }
    @Override
    public void paintComponent (Graphics g) {
        super.paintComponent(g);

    }

    private void drawGrid (Graphics2D graphics) {
        double width = this.getWidth() - 2 * margin;
        double height = this.getHeight() - 2 * margin;
        Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);
        graphics.setColor(marginColor);

        CellPositionToPixelConverter pixelConverter = new CellPositionToPixelConverter(rectangle, colorGrid, margin);
        drawCells(graphics, colorGrid, pixelConverter);
    }


    private static void drawCells (Graphics2D g, CellColorCollection cc, CellPositionToPixelConverter cp) {
        for (CellColor c : cc.getCells()) {
            Rectangle2D rect = cp.getBoundsForCell(c.cellPosition());
            if (c.color() == null) {
                g.setColor(FRAMECOLOR);

            } else {
                g.setColor(c.color());
            }
            g.fill(rect);


        }
    }}

