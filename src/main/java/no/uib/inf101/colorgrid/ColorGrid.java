package no.uib.inf101.colorgrid;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements  IColorGrid {

    int rows, columns;
    List<ArrayList<Color>> grid;


    public ColorGrid (int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.grid = new ArrayList<ArrayList<Color>>();

        for (int i = 0; i < this.rows; i++) {
            grid.add(new ArrayList <> ());
            for (int j = 0; j < this.cols(); j++) {
                grid.get(i).add(null);
            }
        }

    }

    @Override
    public List<CellColor> getCells() {
        List <CellColor> cell = new ArrayList<>();
        for (int i = 0; i < this.rows(); i++) {
            for (int j = 0; j < this.cols(); j++) {
                CellPosition cellPosition = new CellPosition(i, j);
                cell.add(new CellColor (cellPosition, get(cellPosition)));
            }
        }
        return cell;
    }

    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.columns;
    }

    @Override
    public Color get(CellPosition pos) {
        return grid.get(pos.row()).get(pos.col());
    }

    @Override
    public void set(CellPosition pos, Color color) {
        grid.get(pos.row()).set(pos.col(), color);

    }
}
