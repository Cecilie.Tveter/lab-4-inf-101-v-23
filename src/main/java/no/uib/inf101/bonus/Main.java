package no.uib.inf101.bonus;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.gridview.GridView;

import javax.swing.*;
import java.awt.*;

public class Main {
  public static void main(String[] args) {

    GridView gridView = new GridView();
    JFrame jFrame = new JFrame();

    ColorGrid colorGrid = new ColorGrid(3, 4);
    colorGrid.set(new CellPosition(0, 0), Color.red);
    colorGrid.set(new CellPosition(0, 3), Color.BLUE);
    colorGrid.set(new CellPosition(2, 0), Color.yellow);
    colorGrid.set(new CellPosition(2, 3), Color.green);



    jFrame.setContentPane(gridView);

    jFrame.setTitle("BÆSJ");
    jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jFrame.pack();
    jFrame.setVisible(true);
    // Kopier inn main-metoden fra kursnotatene om grafikk her,
    // men tilpass den slik at du oppretter et BeautifulPicture -objekt
    // som lerret.
  }
}
